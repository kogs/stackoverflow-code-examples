import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Answer to: http://stackoverflow.com/questions/30780005/javafx-scrollpane-check-which-components-are-displayed
 * 
 * 
 * @author Marcel Vogel
 *
 */
public class TestScrollPaneVisibleElements extends Application {
	
	private ObservableList<Node> visibleNodes;
	
	public static void main(String[] args) {
		Application.launch(args);
	}
	
	/* (non-Javadoc)
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		visibleNodes = FXCollections.observableArrayList();
		
		ScrollPane pane = new ScrollPane();
		pane.vvalueProperty().addListener((obs) -> {
			checkVisible(pane);
		});
		pane.hvalueProperty().addListener((obs) -> {
			checkVisible(pane);
		});
		


		VBox box = new VBox();
		pane.setContent(box);
		for (int i = 0; i < 30; i++) {
			box.getChildren().add(new Label("Label: " + i));
		}
		StackPane root = new StackPane(pane);
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	private void checkVisible(ScrollPane pane) {
		visibleNodes.setAll(getVisibleElements(pane));
	}

	private List<Node> getVisibleElements(ScrollPane pane) {
		List<Node> visibleNodes = new ArrayList<Node>();
		Bounds paneBounds = pane.localToScene(pane.getBoundsInParent());
		if (pane.getContent() instanceof Parent) {
			for (Node n : ((Parent) pane.getContent()).getChildrenUnmodifiable()) {
				Bounds nodeBounds = n.localToScene(n.getBoundsInLocal());
				if (paneBounds.intersects(nodeBounds)) {
					visibleNodes.add(n);
				}
			}
		}
		return visibleNodes;
	}
	


}
